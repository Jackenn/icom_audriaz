<?php require "inc/config.php"; ?>
<?php require "inc/security.php"; ?>

<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <title>Project Manager -Audriaz</title>
    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    <header class="pm-header">
       <span class="pm-header-title">PROJECT MANAGEMENT</span>
        <a href="logout.php"><span class="pm-header-logout">Logout</span></a>
    </header>

    <section class="pm-section">
        <ul class="pm-section-title">
            <li>
                <span class="pm-section-title-name"> </span>
                <span class="pm-section-title-item">Janvier</span>
                <span class="pm-section-title-item">Février</span>
                <span class="pm-section-title-item">Mars</span>
                <span class="pm-section-title-item">Avril</span>
                <span class="pm-section-title-item">Mai</span>
                <span class="pm-section-title-item">Juin</span>
                <span class="pm-section-title-item">Juillet</span>
                <span class="pm-section-title-item">Aout</span>
                <span class="pm-section-title-item">Septembre</span>
                <span class="pm-section-title-item">Octobre</span>
                <span class="pm-section-title-item">Novembre</span>
                <span class="pm-section-title-item">Décembre</span>
                <span class="pm-section-title-delete"></span>
            </li>
        </ul>

        <ul class="pm-section-content">

            <?php foreach ($projects as $project) :?>
               <li>

                   <span class="pm-section-content-item-name"><?php echo $project['name']?></span>
                   <span class="pm-section-content-item-date">
                       <span class="pm-section-content-item-start">Début :  <?php echo $project['start']?> / </span>
                       <span class="pm-section-content-item-end">Fin :  <?php echo $project['end']?></span>
                   </span>
                   <a href="delete.php?project=<?php echo $project['id'];?>"><span class="pm-section-content-item-delete">Supprimer</span></a>

               </li>
           <?php endforeach ;?>

       </ul>

   </section>

   <footer class="pm-footer">
       <a href="add.php"><span class="pm-footer-add">AJOUTER UN PROJET</span></a>
   </footer>
</body>
</html>
