<?php require "inc/config.php"; ?>

<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <title>Project Manager -Audriaz</title>
  <link rel="stylesheet" href="css/style.css">

</head>

<body>

  <section class="pm-section">
    <form class="add-form" method="post" action="insert.php"">

      <div class="add">
        <span class="add-form-label">Nom</span>
        <span><input name="name" class="add-form-textarea"></input></span>
      </div>

      <div class="add">
        <span class="add-form-label">Début</span>
        <span><input name="start" class="add-form-input" type="date" name="start"></span>
      </div>

      <div class="add">
        <span class="add-form-label">Fin</span>
        <span><input name="end" class="add-form-input" type="date" name="end"></span>
      </div>

      <div class="add-form-send"><input class="add-form-send-button" type="submit" value="OK" /></div>
    </form>
  </section>

</body>
</html>
